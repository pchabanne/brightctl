cp brightctl /usr/local/bin/brightctl && chmod +x /usr/local/bin/brightctl
cp fix-perms-brightness.service /etc/systemd/system/fix-perms-brightness.service && chown root:root /etc/systemd/system/fix-perms-brightness.service
cp fix-perms-brightness.sh /usr/local/sbin/fix-perms-brightness.sh && chown root:root /usr/local/sbin/fix-perms-brightness.sh && chmod +x /usr/local/sbin/fix-perms-brightness.sh

systemctl enable fix-perms-brightness.service
