# Brightctl
Allow user to gain more control on laptop screen brightness on Ubuntu 22.O4.
## Installation
```
git clone https://gitlab.com/pchabanne/brightctl.git
cd brightctl/
sudo chmod +x install.sh
sudo ./install.sh
```
## Documentation
min brightness: 0 (the screen is off)
max brightness: 96000

`brightctl up` to rise brightness by 2000

`brightctl down` to lower brightness by 2000

`brightctl max` to set brightness to 96000

`brightctl min` to set brightness to 1 (minimal brightness with the screen on)

`brightctl off` to turn off the screen (brightness is set to 0)
